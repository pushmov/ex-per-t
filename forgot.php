<?php
session_start();
ob_start();      

include "includes/datalayer.php";

if(isset($_SESSION["ID"]))
{		
	header("Location:user/dashboard.php");
}

$error="";
$email="";


if(isset($_POST["btnSubmit"]))
{
	$row=ForgotPassword($_POST["txtEmail"]);
	if($row!=0)
	{		
		$pwd=$row["password"];
		SendEmail($_POST["txtEmail"],"Password Recovered","This is your password: $pwd");
		$error="Your password is recovered and sent to your email ID";
	}
	else
	{
		$email=$_POST["txtEmail"];
		$error="Oops! This email ID doesn't exists.";
	}
}
?>

<!DOCTYPE HTML>
<html>
	<head>
		<title>Forgot Password | EX-PER-T - Find the advice you were looking for</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<body>

		<!-- Header -->
			<header id="header">
				<div class="inner">
					<a href="index.html" class="logo"><img src="images/main-logo.png" alt="logo"></a>
					<nav id="nav">
						
					</nav>
				</div>
			</header>
			<a href="#menu" class="navPanelToggle"><span class="fa fa-bars"></span></a>

		<section id="main">
				<div class="inner">
					<header class="major special">
						<h1>Forgot Password</h1>
						<p>Kindly fill-up the details to get password in your email ID</p>
					</header>
					
					<!-- Login Form -->
						<section>
						<h5><span><?php echo $error; ?><span></h5>
							<form method="post" action="#">
								<div class="row uniform 50%">
									<div class="6u 12u$(xsmall)">
										<input type="email" name="txtEmail" id="txtEmail" placeholder="Email" required/>
									</div>
									<div class="12u$">
										<ul class="actions">
											<li><input type="submit" value="Submit" name="btnSubmit" id="btnSubmit" class="special" /></li>													
										</ul>										
									</div>								
								</div>
							</form>
						</section>
				</div>
		</section>			

		<!-- Footer -->
			<section id="footer">
				<div class="inner">					
					<div class="copyright">
						&copy; Copyright ex-per-t.com. All Rights Reserved.
					</div>
				</div>
			</section>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>