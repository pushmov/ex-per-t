<?php
include "connection.php";

function AdminLogin($username,$pwd)
{
	global $conn;
	
	$securePWD=md5($pwd);
	
	$sql="select ID from admin where username='$username' and password='$securePWD' and status=1";
	
	$result = $conn->query($sql);
	if ($result->num_rows > 0) 
	{
		while($row = $result->fetch_assoc()) 
		{
			return $row;
		}
	}
	else
	{
		return 0;
	}
	
	$conn->close();
}

function ListOfAllUsers()
{
	global $conn;	
	$data=array();
	
	$sql="select user.ID,avatar.name as avatar,user.email,user.status from user inner join avatar on user.avatar=avatar.ID";
	
	$result = $conn->query($sql);
	if ($result->num_rows > 0) 
	{
		while($row = $result->fetch_assoc()) 
		{
			$data[]= $row;
		}
		return $data;
	}
	else
	{
		return 0;
	}
	
	$conn->close();
}

function ListOfAllBookings()
{
	global $conn;	
	$data=array();
	
	$sql="select distinct user.ID,user.firstname,user.email from user inner join booking on booking.bookingby=user.ID";
	
	$result = $conn->query($sql);
	if($result->num_rows > 0) 
	{
		while($row = $result->fetch_assoc()) 
		{
			$sqlBooking="SELECT booking.BookingBy,booking.BookingTo,booking.BookTime,booking.BookScheduleDate,booking.BookScheduleTime,booking.BookingStatus,user.firstname,user.email FROM booking inner join user on booking.BookingTo=user.ID and booking.BookingBy=".$row["ID"];
	
			$resultBooking = $conn->query($sqlBooking);
			if ($resultBooking->num_rows > 0) 
			{
				while($bookingData = $resultBooking->fetch_assoc()) 
				{
					$data[] = array("BookingBy" => $row['ID'], "BookingByEmail" => $row['email'], "BookingByName" =>  $row['firstname'],"BookingToID" =>  $bookingData['BookingTo'],"BookingToEmail" =>  $bookingData['email'],"BookingToName" =>  $bookingData['firstname'],"BookScheduleDate" => $bookingData['BookScheduleDate'],"BookScheduleTime" => $bookingData['BookScheduleTime'],"BookingStatus" => $bookingData['BookingStatus']);
				}				
			}
		}
		
		return $data;
	}
	else
	{
		return 0;
	}
	
	$conn->close();
}
?>