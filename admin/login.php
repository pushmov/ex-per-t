<?php
session_start();
ob_start();

include "../includes/adminlayer.php";

if(isset($_SESSION["adminID"]))
{		
	header("Location:index.php");
}

$error="";
if(isset($_POST["btnLogin"]))
{
	$row=AdminLogin($_POST["txtUsername"],$_POST["txtPwd"]);
	if($row!=0)
	{		
		$_SESSION["adminID"]=$row["ID"];
		header("Location:index.php");
	}
	else
	{
		$error="Oops! Authentication failed. Please try again.";
	}
}
//Japan@675#
?>

<!DOCTYPE HTML>
<html>
	<head>
		<title>Admin Login | EX-PER-T - Find the advice you were looking for</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="../assets/css/main.css" />
	</head>
	<body>

		<!-- Header -->
			<header id="header">
				<div class="inner">
					<a href="index.html" class="logo"><img src="../images/main-logo.png" alt="logo"></a>
					<nav id="nav">
											
					</nav>
				</div>
			</header>
			<a href="#menu" class="navPanelToggle"><span class="fa fa-bars"></span></a>

			<section id="main">
				<div class="inner">
					<header class="major special">
						<h1>Admin Login</h1>
						<p>Kindly fill-up the details to login into administration area</p>
					</header>
					
					<!-- Login Form -->
					<section>
						<h5><span><?php echo $error; ?><span></h5>
							<form method="post" action="#">
								<div class="row uniform 50%">
									<div class="6u 12u$(xsmall)">
										<input type="text" name="txtUsername" id="txtUsername" value="" placeholder="Username" required/>
									</div>
									<div class="6u$ 12u$(xsmall)">
										<input type="password" name="txtPwd" id="txtPwd" value="" placeholder="Password" required/ />
									</div>
									
									<div class="6u 12u$(small)">
										<input type="checkbox" id="chkRemember" name="chkRemember" checked>
										<label for="chkRemember">Keep me signed in</label>
									</div>
									<div class="12u$">
										<ul class="actions">
											<li><input type="submit" value="Login" name="btnLogin" id="btnLogin" class="special" /></li>													
										</ul>
									</div>
								</div>
							</form>
					</section>
				</div>
			</section>			

		<!-- Footer -->
			<section id="footer">
				<div class="inner">					
					<div class="copyright">
						&copy; Copyright ex-per-t.com. All Rights Reserved.
					</div>
				</div>
			</section>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>