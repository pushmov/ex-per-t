<!-- Footer -->
			<section id="footer">
				<div class="inner">					
					<div class="copyright">
						&copy; Copyright ex-per-t.com. All Rights Reserved.
					</div>
				</div>
			</section>

		<!-- Scripts -->
			<script src="../assets/js/jquery.min.js"></script>
			<script src="../assets/js/skel.min.js"></script>
			<script src="../assets/js/util.js"></script>
			<script src="../assets/js/main.js"></script>