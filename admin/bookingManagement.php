<?php
session_start();
ob_start();

include "../includes/adminlayer.php";

if(isset($_SESSION["adminID"]))
{		
	header("Location:login.php");
}
	
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Booking Management | EX-PER-T - Find the advice you were looking for</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="../assets/css/main.css" />
	</head>
	<body>

		<!-- Header -->
		<?php include "adminHeader.php"; ?>		

		<section id="main">
				<div class="inner">
					<header class="major special">
						<h1>Booking Notifications</h1>
						<p>Details of all the booking notifications</p>
					</header>
					
						<section>
							<div class="table-wrapper">
								<table>
									<thead>
										<tr>
											<th>Name</th>
											<th>Email</th>
											<th></th>
											<th>Booking To</th>
											<th>Booking To Email</th>
											<th>Booked Date</th>
											<th>Booked Time</th>
											<th>Booking Status</th>											
										</tr>
									</thead>
									<tbody>
									<?php
									$row=ListOfAllBookings();
									if(!empty($row))
									{
										foreach($row as $data)
										{
											echo "<tr><td>".$data['BookingByName']."</td><td>".$data['BookingByEmail']."</td><td>wants to book</td><td>".$data['BookingToName']."</td><td>".$data['BookingToEmail']."</td><td>".$data['BookScheduleDate']."</td><td>".$data['BookScheduleTime']."</td>";
											if($data['BookingStatus']==1)
											{
												echo "<td style='color:#008000'>Booked</td>";
											}
											else
											{
												echo "<td style='color:#FF0000'>Pending</td>";
											}
											
											echo "</tr>";
										}
									}
									
									?>
									</tbody>
									
								</table>
							</div>
						
						</section>
				</div>
		</section>					
		<!-- Footer -->
			<?php include "footer.php"; ?>

	</body>
</html>