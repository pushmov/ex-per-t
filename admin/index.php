<?php

include "../includes/adminlayer.php";



if(isset($_SESSION["adminID"]))

{		

	header("Location:login.php");

}

	

?>

<!DOCTYPE HTML>

<html>

	<head>

		<title>Admin Dashboard | EX-PER-T - Find the advice you were looking for</title>

		<meta charset="utf-8" />

		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />

		<link rel="stylesheet" href="../assets/css/main.css" />

	</head>

	<body>



		<!-- Header -->

			<?php include "adminHeader.php"; ?>		



		<section id="main">

				<div class="inner">

					<header class="major special">

						<p>Welcome <b>Admin</b></p>

					</header>

					

						<section>

						<h3>List of all registered users</h3>

							

						</section>

						

						<section>

							<div class="table-wrapper">

								<table>

									<thead>

										<tr>

											<th>ID</th>

											<th>Profile</th>

											<th>Email</th>

											<th>Status</th>	

											<th>View Profile</th>

											<th>Edit</th>

											<th>Delete</th>											

										</tr>

									</thead>

									<tbody>

									<?php

									$row=ListOfAllUsers();

									if(!empty($row))

									{

										foreach($row as $data)

										{

											echo "<tr><td>".$data['ID']."</td><td><img src='../avatars/".$data["avatar"]."' alt='avatar' width='50' height='auto' /></td><td>".$data['email']."</td>";

											if($data['status']==1)

											{

												echo "<td style='color:#008000'>Activated</td>";

											}

											else

											{

												echo "<td style='color:#FF0000'>Deactivated</td>";

											}

											echo "<td><img src='../images/view.png'></td><td><img src='../images/edit.png'></td><td><img src='../images/erase.png'></td></tr>";											

										}

									}

									

									?>

									</tbody>

									

								</table>

							</div>

						

						</section>

				</div>

		</section>					

		<!-- Footer -->

			<?php include "footer.php"; ?>



	</body>

</html>