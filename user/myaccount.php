<?php
ob_start();
session_start();


include "../includes/datalayer.php";

if(!isset($_SESSION["ID"]))
{
	header("Location:../login.php");
}
else
{
	$error="";
	if(isset($_POST["btnUpdate"]))
	{
		$result=UpdateRegisterUser($_SESSION["ID"],$_POST["txtfName"],$_POST["txtlName"],$_POST["txtContact"],$_POST["txtState"],$_POST["txtCountry"]);
		
		if($result==TRUE)
		{			
			$error="Congratulations! Account updated successfully";
		}	
		else
		{			
			$error="Oops! Something went wrong. Please check and try again";
		}	
	}


	$row=GetRegisteredUser($_SESSION["ID"]);
	if(count($row!=0))
	{
		$firstName=$row["firstname"];
		$lastName=$row["lastname"];
		$email=$row["email"];
		$phone=$row["phone"];
		$state=$row["state"];
		$country=$row["country"];
	}
	else
	{
		header("Location:../login.php");
	}
}
?>
<!DOCTYPE HTML>
<html>
	<head>
	<link rel=�shortcut icon� href=�http://www.ex-per-t.com/favicon.ico� type=�image/icon� />
	<link rel=�icon� href=�http://www.ex-per-t.com/favicon.ico� type=�image/icon� />
		<title>My Account | EX-PER-T - Find the advice you were looking for</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="../assets/css/main.css" />
	</head>
	<body>

		<!-- Header -->
			<?php include "header.php"; ?>
		

		<section id="main">
				<div class="inner">
					<header class="major special">
						<h1>My Account</h1>
						<p>Edit your account information</p>
					</header>
					
						<section>
						<h5><span><?php echo $error; ?><span></h5>
							<form method="POST">
								<div class="row uniform 50%">
									<div class="6u 12u$(xsmall)">
										<input type="text" name="txtfName" id="txtfName" value="<?php echo $firstName; ?>" placeholder="First Name" required />
									</div>
									<div class="6u$ 12u$(xsmall)">
										<input type="text" name="txtlName" id="txtlName" value="<?php echo $lastName; ?>" placeholder="Last Name" required />										
									</div>
									<div class="6u 12u$(xsmall)">
										<input type="email" name="txtEmail" id="txtEmail" value="<?php echo $email; ?>" placeholder="Email" required disabled/>										
									</div>
									<div class="6u$ 12u$(xsmall)">
										<input type="text" name="txtContact" id="txtContact" value="<?php echo $phone; ?>" placeholder="Phone" required/>										
									</div>
									<div class="6u 12u$(xsmall)">
										<input type="text" name="txtState" id="txtState" value="<?php echo $state; ?>" placeholder="State/Province" required/>
									</div>
									<div class="6u$ 12u$(xsmall)">
										<input type="text" name="txtCountry" id="txtCountry" value="<?php echo $country; ?>" placeholder="Country" required/>										
									</div>
									<div class="12u$">
										<ul class="actions">
											<li><input type="submit" value="Update" name="btnUpdate" id="btnUpdate" class="special" /></li>											
										</ul>
									</div>
								</div>
							</form>
						</section>
				</div>
		</section>			

		<!-- Footer -->
			<?php include "footer.php"; ?>

	</body>
</html>