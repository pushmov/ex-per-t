<?php
session_start();
ob_start();

include "../includes/datalayer.php";

if(!isset($_SESSION["ID"]))
{   
	header("Location:../login.php");
}
else
{
	$error="";
	
	$row=GetRegisteredUser($_SESSION["ID"]);
	if(count($row!=0))
	{
		$firstName=$row["firstname"];	
		$avatar=$row["Name"];		
	}
	else
	{
		header("Location:../login.php");
	}	
}

if(isset($_POST["btnSearch"]))
{
	if(!empty($_POST["keyword"]))
	{
		unset($_SESSION["LastSearch"]);
		$keyword=trim($_POST["keyword"]);
	}	
}
else if(isset($_SESSION["LastSearch"]))
{
	$keyword=trim($_SESSION["LastSearch"]);
	unset($_SESSION["LastSearch"]);
}
else
{
	$keyword="";
}
	
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Dashboard | EX-PER-T - Find the advice you were looking for</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="../assets/css/main.css" />
	</head>
	<body>

		<!-- Header -->
			<?php include "header.php"; ?>		

		<section id="main">
				<div class="inner">
					<header class="major special">
						<p>Welcome <b><?php echo $firstName; ?></b> You will be identified on EX-PER-T as:</p>
						<img src="../avatars/<?php echo $avatar; ?>" alt="" width="10%" height="auto" class="profile_img"/>						
					</header>
					
						<section>
						<h3>You can always modify your search here:</h3>
							<form method="POST">
								<div class="row uniform 50%">								
									<div class="12u$">
										<input type="text" name="keyword" id="keyword" value="<?php echo $keyword; ?>" placeholder="" required />
									</div>
									<div class="12u$">
										<ul class="actions">
											<li><input type="submit" value="Search" name="btnSearch" id="btnSearch" class="special" /></li>											
										</ul>
									</div>
								</div>
							</form>
						</section>
						
						<section>
							<h4>Search Results<?php if(!empty($keyword)){ echo "<strong class='profile_search_text'> - ". $keyword.'</strong>'; } ?></h4>
							<?php 
							if(!empty($keyword))
							{
								$row=SearchProfileDetails($keyword,$_SESSION["ID"]);
								if(!empty($row))
								{
									foreach($row as $data)
									{
										echo "<div class='row'><blockquote>";
										?>
											<div class="profile_search_biography">
											<div class="profile_search_image"><img src="../avatars/<?php echo $data["name"]; ?>" alt="avatar" />
											<h6><?php if(!empty($data["country"])){ echo $data["state"]." ( ". $data["country"].' )'; } ?></h6>
											<form method="POST" action="booktime.php">
											<input type="hidden" value="<?php echo $data["UserID"]; ?>" id="hdnProfileID" name="hdnProfileID">
											<input type="submit" id="btnBookTime" name="btnBookTime" value="Book EX-Per-T session" class="button special icon fa fa-calendar">
											</form></div></div>
											
											<div class="profile_search_biography"><div class='reviews_block'><strong class="reviews">Reviews :</strong> No review yet<strong class="ranking">Ranking :</strong> No ranking yet</div></div>
											<div class="profile_search_biography">
											<h5>A bit about this user</h5>
											<?php echo $data["biography"]; ?></div>
											<div class="profile_search_biography">
											<h5>This user is willing to share knowledge in these topics:</h5><?php echo $data["expertie_01"].', '.$data["expertie_02"].', '.$data["expertie_03"]; ?></div>
											
										<?php
										echo "</blockquote></div>";
									}		
								}	
								else
								{
									echo "<blockquote>Sorry! No result found!</blockquote>";
								}	
							}
							else
							{
								echo "<blockquote>Sorry! No result found!</blockquote>";
							}
							 ?>
						
						</section>
				</div>
		</section>					
		<!-- Footer -->
			<?php include "footer.php"; ?>

	</body>
</html>