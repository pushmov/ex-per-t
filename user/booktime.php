<?php

ob_start();

session_start();





include "../includes/datalayer.php";



if(!isset($_SESSION["ID"]))

{

	header("Location:../login.php");

}



$msg="";



if(isset($_POST["btnBookTimeSlot"]))

{

	if(!empty($_POST["hdnProfileID"]))

	{

		$result=InsertBooking($_SESSION["ID"],$_POST["hdnProfileID"],$_POST["datepickerListAppointments"],$_POST["timePick"]);		

		if($result==2)

		{			

			$msg= "<h5><span>Thank you for being part of the EX-PER-T community. We will check the availability with the other user and get back to you ASAP.</span></h5>";	

			$row=GetRegisteredUser($_POST["hdnProfileID"]);

			if(count($row!=0))

			{

				$firstName=$row["firstname"];

			}

			$row=GetRegisteredUser($_SESSION["ID"]);

			if(count($row!=0))

			{

				$regfirstName=$row["firstname"];

				$email=$row["email"];

			}

			SendEmail("contactus@ex-per-t.com","New EX-Per-T session booking","User($regfirstName and $email and $datepickerListAppointments tried to book an ex-per-t session with $firstName");

		}

		else

	    {

			$msg= "<h5><span>Oops! Something went wrong. Please check and try again.</span></h5>";

		}

	}							

}

else

{

	if(!empty($_POST["hdnProfileID"]))

	{

		$result=CheckBooking($_SESSION["ID"],$_POST["hdnProfileID"]);

		

		if($result==1)

		{

			$msg= "<h5><span>You already have booked time for this user. Please try with another</h5>";

		}

		else if($result==2)

		{			

			$row=GetRegisteredUser($_POST["hdnProfileID"]);

			if(count($row!=0))

			{

				$firstName=$row["firstname"];

			}

			$msg= "<h5><span>Please select time and date for your EX-PER-T session. We will check availability with $firstName and notify you as soon as possible.<span><div><form method='POST' action=booktime.php><input id='datepickerListAppointments' name='datepickerListAppointments' class='calendar_field' type='text' value='".date("m/d/Y")."'><input class='timepicker calendar_field' name='timePick' id='timePick' type='text' value='9:00 am'><input type='hidden' value='".$_POST["hdnProfileID"] ."' id='hdnProfileID' name='hdnProfileID'><input type='submit' id='btnBookTimeSlot' name='btnBookTimeSlot' value='Book EX-Per-T session Now' class='button special icon fa fa-calendar'></form></div></h5>";

		}

		else if($result==0)

		{	

			$msg="<script>$(document).ready(function () { $('#modal').dialog({title:'Incomplete Profile'}); }); </script>";			

		}	

		

	}

	else

	{

		header("Location:dashboard.php");

	}

}



?>

<!DOCTYPE HTML>

<html>

	<head> 

		<title>Book EX-Per-T Session | EX-PER-T - Find the advice you were looking for</title>

		<meta charset="utf-8" />

		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />

		<link rel="stylesheet" href="../assets/css/main.css" />

		

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>

		<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.js"></script>

		<script src="calendar/jquery.timepicker.js"></script>



		<link href="calendar/jquery-ui.css" rel="stylesheet"/>

		<link href="calendar/jquery.timepicker.css" rel="stylesheet"/>



	</head>

	<body>



		<!-- Header -->

			<?php include "header.php"; ?>		



		<section id="main">

				<div class="inner">

					<script>

						var fakeDisabledTimes = [

							<?php

							$row=ShowAllBookingDetailsIntoCalendar();

							if(!empty($row))

							{

								

								foreach($row as $data)

								{

									$removeAM=str_replace("am","",$data["BookScheduleTime"]);

									$removePM=str_replace("pm","",$removeAM);

									$removeSpace=str_replace(" ","",$removePM);



									$schduleDate=$data["BookScheduleDate"]." ".$removeSpace;

									$currDate=date("m/d/Y H:i:s",strtotime($schduleDate));

									$actualSchedule=date("Y-m-d H:i:s",strtotime($currDate));

									echo "'".$actualSchedule."',";

								}

							}



							?>

						   

						];



						$(document).ready(function(){

						  $( "#datepickerListAppointments" ).datepicker({

							minDate:'0',

							beforeShowDay:

							function(dt){

							  // need to disable days other than tuesday and wednesday too.

							  return [dt.getDate() === dt.getDate(), ""];

							},

							onSelect : function(dateText){

							  //should disable/enable timepicker times from here!

							  // parse selected date into moment object

							  var selDate = moment(dateText, 'MM/DD/YYYY');

							  // init array of disabled times

							  var disabledTimes = [];

							  // for each appoinment returned by the server

							  for(var i=0; i<fakeDisabledTimes.length; i++){

								// parse appoinment datetime into moment object

								var m = moment(fakeDisabledTimes[i]);

								// check if appointment is in the selected day

								if( selDate.isSame(m, 'day') ){

								  // create a 30 minutes range of disabled time

								  var entry = [

									m.format('h:mm a'),

									m.clone().add(60, 'm').format('h:mm a')

								  ];

								  // add the range to disabled times array

								  disabledTimes.push(entry);

								}

							  }

							  // dinamically update disableTimeRanges option

							  $('input.timepicker').timepicker('option', 'disableTimeRanges', disabledTimes);

							}

						  });



						  $('input.timepicker').timepicker({

							timeFormat: 'h:i a',

							interval: 90,

							minTime: '9:00am',

							maxTime: '7:00pm',

							defaultTime: '9',

							startTime: '9:00',

							dynamic: false,

							dropdown: true,

							scrollbar: false                

						  });



						});



					</script>

					<header class="major special">

						<h1>Book EX-Per-T Session</h1>

						<?php

							echo $msg; 

						?>		

						

						

					</header>

					

						<section>

							<div class='row'>

							<blockquote>

								<?php 

								if(!empty($_POST["hdnProfileID"]))

								{

									$firstName='';

									$row=GetRegisteredUser($_POST["hdnProfileID"]);

									if(count($row!=0))

									{

										echo '<div class="profile_search_image"><img src="../avatars/'.$row["Name"].'" alt="avatar" /></div>';	

										$firstName=$row["firstname"];

									}

									

									$row=GetProfileDetails($_POST["hdnProfileID"]);

									if(count($row!=0))

									{

										echo '<div class="profile_search_biography"><h5>A bit about this user</h5>'. $row["biography"].'</div>';

										echo '<div class="profile_search_biography"><h5>This user is willing to share knowledge in these topics:</h5>'.$row["Expertie_01"].', '.$row["Expertie_02"].', '.$row["Expertie_03"].'</div>';										

									}								

								}

								else

								{

									header("Location:dashboard.php");

								}

								

								?>							

							</blockquote></div>

						</section>

				</div>

		</section>			

		

		<div id="modal" style="display: none;margin-top:15px;">

			<h5><span>Please complete your profile before requesting an EX-PER-T session.<p style='margin-top:20px;'><a href='myprofile.php' class='button special'>Complete Your Profile</a></p><span></h5>

		</div>





		<!-- Footer -->

			<?php include "footer.php"; ?>







	</body>

</html>

