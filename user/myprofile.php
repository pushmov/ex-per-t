<?php
ob_start();
session_start();


include "../includes/datalayer.php";

if(!isset($_SESSION["ID"]))
{
	header("Location:../login.php");
}
else
{
	$error="";
	if(isset($_POST["btnUpdate"]))
	{
		$result=InsertProfile($_SESSION["ID"],$_POST["txtAbout"],$_POST["txtQuestion1_1"],$_POST["txtQuestion1_2"],$_POST["txtQuestion1_3"],$_POST["txtQuestion2_1"],$_POST["txtQuestion2_2"],$_POST["txtQuestion2_3"]);
		
		if($result==TRUE)
		{			
			$error="Congratulations! Profile updated successfully";
		}	
		else
		{			
			$error="Oops! Something went wrong. Please check and try again";
		}	
	}


	$row=GetRegisteredUser($_SESSION["ID"]);
	if(count($row!=0))
	{
		$avatar=$row["Name"];		
	}
	else
	{
		header("Location:../login.php");
	}
	
	$row=GetProfileDetails($_SESSION["ID"]);
	if(count($row!=0))
	{
		$biography=$row["biography"];
		$willing_01=$row["Willing_01"];
		$willing_02=$row["Willing_02"];
		$willing_03=$row["Willing_03"];
		$expertie_01=$row["Expertie_01"];		
		$expertie_02=$row["Expertie_02"];		
		$expertie_03=$row["Expertie_03"];		
	}
	else
	{
		header("Location:http://www.ex-per-t.com/");
	}
}
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>My Profile | EX-PER-T - Find the advice you were looking for</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="../assets/css/main.css" />
		<link rel=�shortcut icon� href=�http://www.ex-per-t.com/new/favicon.ico� type=�image/icon� />
		<link rel=�icon� href=�http://www.ex-per-t.com/new/favicon.ico� type=�image/icon� />
	</head>
	<body>

		<!-- Header -->
			<?php include "header.php"; ?>
		

		<section id="main">
				<div class="inner">
					<header class="major special">
						<h1>My Profile</h1>
						<p>Note: All the fields should be filled before requesting an EX-PER-T session.</p>
					</header>
					
						<section>
						<h5><span><?php echo $error; ?><span></h5>
							<form method="POST">
								<div class="row uniform 50%">
									<div class="12u$" style="text-align:center;"><span><img src="../avatars/<?php echo $avatar; ?>" alt="" class="profile_img"/></span></div>
									<div class="12u$" style="text-align:center;"><span><h5>Visible in public search</h5></span></div>
									<div class="12u$">										
										<textarea maxlength="260" <?php if(empty($biography)){ echo 'style="border-color:#FD0000;"'; } ?> name="txtAbout" id="txtAbout" placeholder="Tell us a bit about yourself and how do you define yourself (260 characters)" rows="3" required><?php echo $biography; ?></textarea>
									</div>
									<div class="6u 12u$(xsmall)">			
										<p>What topics would you like to get an EX-PER-T session on?</p>	
										<input <?php if(empty($willing_01)){ echo 'style="border-color:#FD0000;"'; } ?> type="text" maxlength="40" name="txtQuestion1_1" id="txtQuestion1_1" value="<?php echo $willing_01;  ?>" placeholder="[ Maximum 40 characters ]" required />
										<input <?php if(empty($willing_02)){ echo 'style="border-color:#FD0000;"'; } ?> type="text" maxlength="40" name="txtQuestion1_2" id="txtQuestion1_2" value="<?php echo $willing_02;  ?>" placeholder="[ Maximum 40 characters ]" required />
										<input <?php if(empty($willing_03)){ echo 'style="border-color:#FD0000;"'; } ?> type="text" maxlength="40" name="txtQuestion1_3" id="txtQuestion1_3" value="<?php echo $willing_03;  ?>" placeholder="[ Maximum 40 characters ]" required />
									</div>
									<div class="6u$ 12u$(xsmall)">
										<p>What topics would you be able to give others an EX-PER-T session on? </p>
										<input <?php if(empty($expertie_01)){ echo 'style="border-color:#FD0000;"'; } ?> type="text" maxlength="40" name="txtQuestion2_1" id="txtQuestion2_1" value="<?php echo $expertie_01; ?>" placeholder="[ Maximum 40 characters ]" required />
										<input <?php if(empty($expertie_02)){ echo 'style="border-color:#FD0000;"'; } ?> type="text" maxlength="40" name="txtQuestion2_2" id="txtQuestion2_2" value="<?php echo $expertie_02; ?>" placeholder="[ Maximum 40 characters ]" required />
										<input <?php if(empty($expertie_03)){ echo 'style="border-color:#FD0000;"'; } ?> type="text" maxlength="40" name="txtQuestion2_3" id="txtQuestion2_3" value="<?php echo $expertie_03; ?>" placeholder="[ Maximum 40 characters ]" required />										
									</div>
									
									<div class="12u$">
										<ul class="actions">
											<li><input type="submit" value="Update" name="btnUpdate" id="btnUpdate" class="special" /></li>											
										</ul>
									</div>
								</div>
							</form>
						</section>
				</div>
		</section>			

		<!-- Footer -->
			<?php include "footer.php"; ?>

	</body>
</html>