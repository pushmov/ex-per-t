<?php
session_start();
ob_start();

include "includes/datalayer.php";

if(isset($_SESSION["ID"]))
{
	header("Location:user/dashboard.php");
}

$error="";
$message="";
if(isset($_POST["btnRegister"]))
{
	if($_POST["txtPwd"]==$_POST["txtConfirmPwd"])
	{
		$result=RegisterUser($_POST["txtEmail"],$_POST["txtPwd"],$_POST["txtName"]);
		if($result!=FALSE)
		{
			if($result=="EXIST")
			{
				$error="Oops! Email already exists. Please try with another";
			}
			else
			{
				$_SESSION["ID"]=$result;
				$error="Thank you for registering. You can now browse for different topics of EX-PER-T using the search bar";
				
				echo '<script type="text/javascript"> function Redirect() {  window.location="user/dashboard.php"; } setTimeout("Redirect()", 3000); </script>';
			}	
		}
		else
		{			
			$error="Oops! Something went wrong. Please check and try again";
		}
	}
	else
	{
		$error="Oops! Password doesn't match";
	}
}
else
{
	if(!empty($_POST["keyword"]))
	{
		$_SESSION["LastSearch"]=$_POST["keyword"];
	}
}
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Registration | EX-PER-T - Find the advice you were looking for</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<body>

		<!-- Header -->
			<header id="header">
				<div class="inner">
					<a href="index.html" class="logo"><img src="images/main-logo.png" alt="logo"></a>
					<nav id="nav">
						
					</nav>
				</div>
			</header>
			<a href="#menu" class="navPanelToggle"><span class="fa fa-bars"></span></a>

		<section id="main">
				<div class="inner">
					<header class="major special">
						<h1>Registration Form</h1>
						<p>We'd like to explain here that this is only an initial registration in order to search for consulting / or  give one.<br>
						In order to book an hour with a consultant, verification is required.</p>
					</header>
					
					<!-- Login Form -->
						<section>
						<h5><span><?php echo $error; ?><span></h5>
							<form method="POST">
								<div class="row uniform 50%">
									<div class="6u 12u$(xsmall)">
										<input type="text" name="txtName" id="txtName" value="" placeholder="Name" required />
									</div>
									<div class="6u$ 12u$(xsmall)">
										<input type="email" name="txtEmail" id="txtEmail" value="" placeholder="Email" required/>										
									</div>
									<div class="6u 12u$(xsmall)">
										<input type="password" name="txtPwd" id="txtPwd" value="" placeholder="Password" onkeyup="CheckPasswordStrength(this.value)" required/>
										 <span id="password_strength"></span>
										 <script type="text/javascript">
											function CheckPasswordStrength(password) {
												var password_strength = document.getElementById("password_strength");
										 
												//TextBox left blank.
												if (password.length == 0) {
													password_strength.innerHTML = "";
													return;
												}
										 
												//Regular Expressions.
												var regex = new Array();
												regex.push("[A-Z]"); //Uppercase Alphabet.
												regex.push("[a-z]"); //Lowercase Alphabet.
												regex.push("[0-9]"); //Digit.
												regex.push("[$@$!%*#?&]"); //Special Character.
										 
												var passed = 0;
										 
												//Validate for each Regular Expression.
												for (var i = 0; i < regex.length; i++) {
													if (new RegExp(regex[i]).test(password)) {
														passed++;
													}
												}
										 
												//Validate for length of Password.
												if (passed > 2 && password.length > 8) {
													passed++;
												}
										 
												//Display status.
												var color = "";
												var strength = "";
												switch (passed) {
													case 0:
													case 1:
														strength = "Weak";
														color = "red";
														break;
													case 2:
														strength = "Good";
														color = "darkorange";
														break;
													case 3:
													case 4:
														strength = "Strong";
														color = "green";
														break;
													case 5:
														strength = "Very Strong";
														color = "darkgreen";
														break;
												}
												password_strength.innerHTML = strength;
												password_strength.style.color = color;
											}
										</script>
									</div>
									<div class="6u$ 12u$(xsmall)">
										<input type="password" name="txtConfirmPwd" id="txtConfirmPwd" value="" placeholder="Confirm Password" required/>										
									</div>
									<div class="12u$">
										<ul class="actions">
											<li><input type="submit" value="Register" name="btnRegister" id="btnRegister" class="special" /></li>	
											<li>Already have an account? <a href="login.php">Login Now</a></li>		
										</ul>
									</div>
								</div>
							</form>
						</section>
				</div>
		</section>			
		
		

		<!-- Footer -->
			<section id="footer">
				<div class="inner">					
					<div class="copyright">
						&copy; Copyright ex-per-t.com. All Rights Reserved.
					</div>
				</div>
			</section>
			
			


		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>