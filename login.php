<?php
session_start();
ob_start();      

include "includes/datalayer.php";

if(isset($_SESSION["ID"]))
{		
	header("Location:user/dashboard.php");
}

$error="";
$email="";
$pwd="";

if(isset($_POST["btnLogin"]))
{
	$row=Login($_POST["txtEmail"],$_POST["txtPwd"]);
	if($row!=0)
	{		
		$_SESSION["ID"]=$row["ID"];
		header("Location:user/dashboard.php");
	}
	else
	{
		$email=$_POST["txtEmail"];
		$pwd=$_POST["txtPwd"];
		
		$error="Oops! Authentication failed. Please try again.";
	}
}
else
{
	if(!empty($_POST["keyword"]))
	{
		$_SESSION["LastSearch"]=$_POST["keyword"];
	}
}
?>

<!DOCTYPE HTML>
<html>
	<head>
		<title>Login | EX-PER-T - Find the advice you were looking for</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<body>

		<!-- Header -->
			<header id="header">
				<div class="inner">
					<a href="index.html" class="logo"><img src="images/main-logo.png" alt="logo"></a>
					<nav id="nav">
						
					</nav>
				</div>
			</header>
			<a href="#menu" class="navPanelToggle"><span class="fa fa-bars"></span></a>

		<section id="main">
				<div class="inner">
					<header class="major special">
						<h1>Login</h1>
						<p>Kindly fill-up the details to login into website</p>
					</header>
					
					<!-- Login Form -->
						<section>
						<h5><span><?php echo $error; ?><span></h5>
							<form method="post" action="#">
								<div class="row uniform 50%">
									<div class="6u 12u$(xsmall)">
										<input type="email" name="txtEmail" id="txtEmail" value="<?php echo $email; ?>" placeholder="Email" required/>
									</div>
									<div class="6u$ 12u$(xsmall)">
										<input type="password" name="txtPwd" id="txtPwd" value="<?php echo $pwd; ?>" placeholder="Password" required/ />
									</div>
									
									
									<div class="12u$">
										<ul class="actions">
											<li><input type="submit" value="Login" name="btnLogin" id="btnLogin" class="special" /></li>	
											<li>Don't have an account yet? <a href="registration.php">Register Now</a></li>		
										</ul>										
									</div>
								<div class="12u$">
										<ul class="actions">
											<li>Forgot your password? <a href="forgot.php">Click here</a></li>		
										</ul>										
									</div>
								</div>
							</form>
						</section>
				</div>
		</section>			

		<!-- Footer -->
			<section id="footer">
				<div class="inner">					
					<div class="copyright">
						&copy; Copyright ex-per-t.com. All Rights Reserved.
					</div>
				</div>
			</section>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>